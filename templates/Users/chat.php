<?php
/* @var App\View\AppView $this
 *
 */
?>
<html>
<head>
    <meta charset="UTF-8">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <script type="module" src="https://cdn.jsdelivr.net/npm/emoji-picker-element@^1/index.js"></script>

    <!--[if lt IE 8]><!-->
    <link rel="stylesheet" href="">
    <?=$this->Html->css([
        '/themify/themify-icons',
        '/themify/ie7/ie7.css'
    ])?>

    <?=$this->Html->meta('csrf-token',$this->request->getAttribute('csrfToken'));?>
    <script>
        const id = '<?=$this->request->getAttribute('identity')->id?>';
    </script>
</head>
<style>
    .message-container{
        max-width:100%;
        width: 100%;
        margin:auto;
    }
    .chat-img img{
        max-width: 100%;
        height: 3em;
        width: 100%;
    }
    .inbox-channels {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 25%;
        height: 100%;;
        border-right: 1px solid #c4c4c4;
    }
    .inbox-messages {
        border: 1px solid #c4c4c4;
        clear: both;
        overflow: hidden;
        width: 100%;
    }
    .heading-search{
        height: 8%;
        width: 100%;
        overflow:hidden;
        border-bottom:1px solid #c4c4c4;
    }
    .chat-ib h5{
        font-size:15px;
        color:#464646;
        margin:0 0 8px 0;
    }
    .chat-ib h5 span{
        font-size:13px;
        float:right;
    }
    .chat-ib p{
        font-size:14px;
        color:#989898;
        margin:auto;
    }
    .chat-img {
        float: left;
        width: 11%;
    }
    .chat-ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }
    .chat-list{
        overflow:hidden;
        clear:both;
    }
    .chat-list-channels {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
        cursor: pointer;
    }
    .channel-inbox {
        height: 92%;
        overflow-y: auto;
    }
    .active-channel{
        background:#ebebeb;
    }
    .incoming-message{
        height: auto;
        overflow: hidden;
        min-width: 50%;
        display: flex;
        flex-direction: row;
        flex-flow: row;
        flex-wrap: wrap;
        align-items: center;
        justify-content:center;
    }
    .incoming-message-img {
        display: flex;
        width: 8%;
        height: 6em;
        flex-direction: row;
        flex-flow: row;
        flex-wrap: nowrap;
        justify-content: center;
        align-items: center;
    }
    .incoming-message-img img{
        max-width:100%;
        height: 3em;
        width: 3em;
    }
    .received-message{
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }
    .received-message-container{
        width: 30%;
    }
    .received-message-container p{
        background: #0892D0 none repeat scroll 0 0;
        border-radius: 0.3em;
        color:white;
        font-weight: 500;
        font-size: 1em;
        margin: 0;
        padding: 5px 10px 5px 12px;
        min-width: 100%;
        height: auto;
        overflow: hidden;
    }
    .received-message-container .message-date-time {
        color: black;
        display: block;
        font-size: 0.8em;
        margin: 8px 0 0;
    }
    .message-form {
        float: left;
        padding: 0;
        width: 75%;
        height: 100%;
    }
    .sent-message p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0; color:#fff;
        padding: 5px 10px 5px 12px;
        width:100%;
    }
    .outgoing-message{
        overflow:hidden;
        margin:26px 0 26px;
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 60%;
        float: right;
    }
    .sent-message {
        float: right;
        width: 50%;
    }
    .sent-message .message-date-time {
        color: black;
        display: block;
        font-size: 0.8em;
        margin: 8px 0 0;
    }
    .type-message {
        border-top: 1px solid #c4c4c4;
        position: relative;
    }
    .messaging {
        padding: 1.2em;
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: row;
        flex-flow: row;
        flex-wrap: wrap;
        justify-content: flex-start;
        align-items: flex-start;
    }
    .message-history {
        height: 100%;
        /*overflow-y: auto;*/
    }
    .inbox-channels-hide{
        width: 0 !important;
        display: none !important;
        opacity: 0 !important;
    }
    .message-notif{
        background-color: #cb4154;
        display: block;
        left: -95%;
        position: relative;
        z-index: 500;
        border-radius: 50%;
        height: 1.3em;
        width: 1.3em;
        text-align: center;
        color: white;
        font-weight: bolder;
        line-height: 1.35em;
    }
    #emojis{
        display: block !important;
        position: absolute !important;
        z-index: 2300 !important;
        right: 0 !important;
        margin-top: -3em !important;
        bottom:103% !important;
    }
    *{
        font-family: 'Poppins', sans-serif;
        font-weight: 500;
    }
</style>
<body>

<div class="message-container">
    <div class="messaging">
        <div class="inbox-messages">
            <div class="inbox-channels" id="inbox-channels">

                <div class="heading-search bg-secondary">
                    <?=ucwords($this->request->getAttribute('identity')->username)?>
                </div>

                <div class="channel-inbox" id="inbox">

                </div>

            </div>
            <div class="message-form" id="message-form">

                <div class="col-sm-12 col-md-12 col-lg-12 bg-secondary" style="height: 8%; width: 100%;">
                    <div class="row h-100">
                        <div class="col-sm-4 col-md-3 col-lg-3 h-100 d-flex justify-content-start align-items-center">
                            <button type="button" class="btn bg-secondary btn-lg text-white" id="menu-collapse" title="Menu Collapse">
                                <i class="fa fa-list"></i>
                            </button>
                        </div>
                        <div class="col-sm-6 col-md-9 col-lg-9 h-100 d-flex justify-content-end align-items-center">
                            <button type="button" class="btn bg-secondary btn-lg text-white" title="Chat Info">
                                <i class="fa fa-question"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12" style="height: 80%; width: 100%; overflow-y: auto;">
                    <div class="message-history" id="message-history" active-channel="">

                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center" style="height: 12%; width: 100%;">
                    <?=$this->Form->create($messages,['id' => 'form', 'class' => 'w-100'])?>
                    <div class="type-message d-none m-3" id="type-message">
                        <div class="input-message-write">

                            <div class="input-group mb-0">
                                <?=$this->Form->text('message',[
                                    'class' => 'form-control rounded-0',
                                    'id' => 'message',
                                    'placeholder' => ucwords('Type a message'),
                                    'label' => false,
                                    'type' => 'text',
                                    'disabled',
                                    'required',
                                ])?>
                                <?=$this->Form->control('channel_id',[
                                    'type' => 'hidden',
                                    'required',
                                    'disabled'
                                ])?>
                                <?=$this->Form->control('receiver_id',[
                                    'type' => 'hidden',
                                    'required',
                                    'disabled'
                                ])?>
                                <?=$this->Form->control('sender_id',[
                                    'type' => 'hidden',
                                    'required',
                                    'value' => ($this->request->getAttribute('identity')->id),
                                    'disabled'
                                ])?>
                                <?=$this->Form->control('user_id',[
                                    'type' => 'hidden',
                                    'required',
                                    'value' => ($this->request->getAttribute('identity')->id),
                                    'disabled'
                                ])?>

                                <div id="emojis">
                                    <emoji-picker style="display: none;" data-source="https://cdn.jsdelivr.net/npm/emoji-picker-element-data@^1/en/emojibase/data.json"></emoji-picker>
                                </div>

                                <div class="input-group-prepend rounded-0">
                                    <?=$this->Form->button('<i class="ti-face-smile"></i>',[
                                        'type' => 'button',
                                        'class' => 'input-group-text btn btn-info rounded-0',
                                        'escapeTitle' => false,
                                        'id' => 'toggle-emoji'
                                    ])?>
                                </div>
                                <div class="input-group-prepend rounded-0">
                                    <?=$this->Form->button('<i class="fa fa-paper-plane-o"></i>',[
                                        'type' => 'submit',
                                        'class' => 'input-group-text btn btn-info rounded-0',
                                        'escapeTitle' => false
                                    ])?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <?=$this->Form->end();?>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function () {

        var socket = new WebSocket('ws://localhost:8080');

        socket.onopen = function() {
            channels();
//            users();
        };

        socket.onmessage = function(e) {
            const data = JSON.parse(e.data);
            if(data.sender.user_id == id || data.receiver.user_id == id){
                if(parseInt($('#message-history').attr('active-channel')) == parseInt(data.message.channel_id)){
                    $('#channel-text-'+(data.message.channel_id)+'').css({
                        'font-weight':'bolder'
                    }).text(data.message.message);
                    messages(data.message);
                    read(data.message.channel_id);
                    return true;
                }else{
                    $('#channel-text-'+(data.message.channel_id)+'').css({
                        'font-weight':'bolder'
                    }).text(data.message.message);
                    unread(data.message);
//                    chats(data.message.channel_id);
//                    read(data.message.channel_id);
                    return true;
                }
            }
        };

        socket.onclose = function(e) {

        };

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: 'http://localhost/CakePHP/messages/add',
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $('#channel-text-'+(data.message.channel_id)+'').text(data.message.message);
                $('#message-history').attr('active-channel', data.message.channel_id);
                $('#channel-id').val(parseInt(data.message.channel_id));
                socket.send(JSON.stringify(data));
                messages(data.message);
                $('#form')[0].reset();
            }).fail(function (data, status, xhr) {
                console.log(data)
            }).always(function (data, status, xhr) {
                scroll();
            });
        });

        $(document).on('click', 'div.channels', function (e) {
            var dataTarget = $(this).attr('data-target');
            var sender = $(this).attr('sender');
            var receiver = $(this).attr('receiver');

            if($(this).hasClass('active-channel')){
                e.preventDefault();
                return false;
            }else{
                $(document).find('div.channels').not(this).removeClass('active-channel');
                $('#receiver-id').val( (id == sender)? receiver: sender );
                $('#message-history').attr('active-channel', dataTarget);
                $(this).addClass('active-channel');
                chats(dataTarget);
            }

        });

        $('#menu-collapse').click(function (e) {
            const inboxChannel = $('#inbox-channels');
            
            inboxChannel.toggleClass('inbox-channels-hide');
            
            if(inboxChannel.hasClass('inbox-channels-hide')){
                $('#message-form').css({
                    'width':'100%'
                });
            }else{
                $('#message-form').css({
                    'width':'75%'
                });
            }

        });

        $('#toggle-emoji').click(function (e) {
            $('emoji-picker').fadeToggle(100);
        });

        $('emoji-picker').on('emoji-click', function (e) {
            var message = $('#message');
            var value = message.val()+(e.detail.unicode);
            message.val(value);
        });

        function chats(channelId) {
            $.ajax({
                url: 'http://localhost/CakePHP/messages/getMessages/'+(channelId),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#message-history').empty().parent().css({'background' : '#ADD8E6'}).next('div').css({'background' : '#ADD8E6'});
                    $('#channel-id').val(channelId);
                },
            }).done(function (data, status, xhr) {
                $.map(data, function (data, key) {
                    messages(data);
                });
            }).fail(function (data, status, xhr) {
                console.log(data)
            }).always(function (data, status, xhr) {
                $('input').prop('disabled', false);
                $('button[type="submit"]').prop('disabled', false);
                $('#type-message').removeClass('d-none').fadeIn(500);
                $('#message-history').attr('active-channel', parseInt(channelId));
                read(channelId);
            });
        }

        function channels() {
            $.ajax({
                url: 'http://localhost/CakePHP/channels/getChannels',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#inbox').empty();
                },
            }).done(function (data, status, xhr) {
                $.map(data, function (data,key) {
                    $('#inbox').append('<div class="chat-list-channels channels" data-target="'+(data.id)+'" sender="'+(data.sender.user_id)+'" receiver="'+(data.receiver.user_id)+'"> ' +
                        '<div class="chat-list"> ' +
                        '<div class="chat-img">' +
                        '<img src="https://ptetutorials.com/images/user-profile.png"> ' +
                        '</div> <div class="chat-ib"> ' +
                        '<h5>'+( (data.sender.user_id == id)? data.receiver.user.username: data.sender.user.username)+'<span class="chat_date">'+(moment(data.messages.created).format('MMM D'))+'</span><span class="message-notif" id="count-messages-'+(data.id)+'" style="display: '+( (parseInt(data.messages.length))? 'block': 'none' )+';">'+(parseInt(data.messages.length))+'</span></h5> ' +
                        '<p id="channel-text-'+(data.id)+'" style="font-weight: '+( (!data.message.is_read && data.message.user_id != id)? 'bolder': 0)+'; color: black;">'+((data.message.message).substr(0, 30))+'...</p> ' +
                        '</div> ' +
                        '</div> ' +
                        '</div>');
                });
            }).fail(function (data, status, xhr) {
                console.log(data)
            });
        }

        function messages(data) {
            var message = '';
            if(data.user_id == id){
                message = '<div class="outgoing-message">' +
                    '<div class="sent-message"> ' +
                    '<p>'+(data.message)+'</p> ' +
                    '<span class="message-date-time">'+(moment(data.created).format('Y/MM/D'))+'</span> ' +
                    '</div>' +
                    '</div>';
            }else{
                message = '<div class="incoming-message"> ' +
                    '<div class="incoming-message-img"> ' +
                    '<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> ' +
                    '</div> <div class="received-message"> ' +
                    '<div class="received-message-container"> ' +
                    '<p>'+(data.message)+'</p> ' +
                    '<span class="message-date-time">'+(moment(data.created).format('Y/MM/D'))+'</span>' +
                    '</div> ' +
                    '</div> ' +
                    '</div>';
            }
            $('#message-history').append(message);
        }

        function read(channelId) {
            var channelId = channelId;
            $.ajax({
                url: 'http://localhost/CakePHP/messages/read/'+(channelId),
                type: 'POST',
                method: 'POST',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $('#channel-text-'+(channelId)+'').prop('style', false).css({
                    'color':'black'
                });
                $('#count-messages-'+(channelId)+'').prop('style', false).css({
                    'display':'none'
                });
            }).fail(function (data, status, xhr) {
                console.log(data)
            }).always(function (data, status, xhr) {
                scroll();
            });
        }

        function unread(data) {
            var message = data;
            $.ajax({
                url: 'http://localhost/CakePHP/messages/unread/'+(message.channel_id),
                type: 'POST',
                method: 'POST',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $('#channel-text-'+(message.channel_id)+'').prop('style', false).css({
                    'color':'black',
                    'font-weight':'bolder'
                }).text(message.message);
                $('#count-messages-'+(message.channel_id)+'').prop('style', true).css({
                    'display':'block'
                }).text(data.count);
            }).fail(function (data, status, xhr) {
                console.log(data)
            }).always(function (data, status, xhr) {

            });
        }

        function scroll() {
            var scroll = document.querySelector('#message-history').parentNode;
            scroll.scrollTop = scroll.scrollHeight;
        }

        setInterval(function () {
            channels();
        },(1000 * 60));

        function users() {
            $.ajax({
                url: 'http://localhost/CakePHP/users/users',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#inbox').empty();
                },
            }).done(function (data, status, xhr) {
                $.map(data, function (data,key) {
                    $('#inbox').append('<div class="chat-list-channels channels" data-target="channel'+(data.id)+'" sender="'+(id)+'" receiver="'+(data.id)+'"> ' +
                        '<div class="chat-list"> ' +
                        '<div class="chat-img"> ' +
                        '<img src="https://ptetutorials.com/images/user-profile.png"> ' +
                        '</div> <div class="chat-ib"> ' +
                        '<h5>'+( (data.id == id)? data.username: data.username)+'<span class="chat_date">'+(/*moment(data.messages[0].created).format('MMM D')*/null)+'</span></h5> ' +
                        '<p id="channel-text-'+(data.id)+'">'+(/*(data.messages[0].message).substr(0, 30)*/null)+'...</p> ' +
                        '</div> ' +
                        '</div> ' +
                        '</div>')
                });
            }).fail(function (data, status, xhr) {
                console.log(data)
            });
        }

    });
</script>

</body>
</html>