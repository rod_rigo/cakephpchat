<!DOCTYPE html>
<html>
<head>
    <title>WebSocket Chat</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
<div id="chat" style="width: 200px;"></div>
<input type="text" id="message" placeholder="Type a message..." />
<button id="send">Send</button>
<script>
    $(document).ready(function() {
        const name = 'P2';
        var socket = new WebSocket("ws://localhost:8080");

        socket.onopen = function() {
            // Handle WebSocket connection opened.
//            socket.send(JSON.stringify({ type: 'auth', username: 'sample' }));
        };

        socket.onmessage = function(event) {
            // Handle incoming messages and display them in the chat window.
            var data = JSON.parse( event.data );
            if(data.receiver == name){
                $("#chat").append('<p style="float: left; width: 100%; text-align: left;">'+(data.message)+'</p>');
                console.log(event.data);
            }
        };

        socket.onclose = function(event) {
            alert('Web Socket Closed!');
        };

        $("#send").click(function() {
            var message = $('#message');
            socket.send(JSON.stringify({
                type: 'message',
                receiver: 'P1',
                message:  message.val(),
                sender: name }));
            message.empty();
            $("#chat").append('<p style="float: right; width: 100%; text-align: right;">'+( message.val())+'</p>');
        });

    });

</script>
</body>
</html>
