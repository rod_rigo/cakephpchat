<?php
/* @var App\View\AppView $this
 *
 */
?>
<html>
<head>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <script>
        const id = '<?=$this->request->getAttribute('identity')->id?>';
    </script>
</head>
<style>
    .container{max-width:1170px; margin:auto;}
    img{ max-width:100%;}
    .inbox_people {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 40%; border-right:1px solid #c4c4c4;
    }
    .inbox_msg {
        border: 1px solid #c4c4c4;
        clear: both;
        overflow: hidden;
    }
    .top_spac{ margin: 20px 0 0;}


    .recent_heading {float: left; width:40%;}
    .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%;
    }
    .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
    }
    .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
    .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
    }
    .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

    .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
    .chat_ib h5 span{ font-size:13px; float:right;}
    .chat_ib p{ font-size:14px; color:#989898; margin:auto}
    .chat_img {
        float: left;
        width: 11%;
    }
    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people{ overflow:hidden; clear:both;}
    .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
        cursor: pointer;
    }
    .inbox_chat { height: 550px; overflow-y: scroll;}

    .active_chat{ background:#ebebeb;}

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }
    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }
    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }
    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }
    .received_withd_msg { width: 57%;}
    .mesgs {
        float: left;
        padding: 30px 15px 0 25px;
        width: 60%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0; color:#fff;
        padding: 5px 10px 5px 12px;
        width:100%;
    }
    .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
    .sent_msg {
        float: right;
        width: 46%;
    }
    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
    }

    .type_msg {border-top: 1px solid #c4c4c4;position: relative;}
    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 8px;
        width: 33px;
    }
    .messaging { padding: 0 0 50px 0;}
    .msg_history {
        height: 516px;
        overflow-y: auto;
    }
    *{
        font-family: 'Poppins', sans-serif;
        font-weight: 500;
    }
</style>
<body>
<div class="container">
    <h3 class=" text-center">Messaging</h3>
    <div class="messaging">
        <div class="inbox_msg">
            <div class="inbox_people">
                <div class="headind_srch">
                    <div class="recent_heading">
                        <h4>Recent</h4>
                    </div>
                    <div class="srch_bar">
                        <div class="stylish-input-group">
                            <input type="text" class="search-bar"  placeholder="Search" >
                            <span class="input-group-addon">
                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                </span> </div>
                    </div>
                </div>
                <div class="inbox_chat" id="inbox">

                </div>
            </div>
            <div class="mesgs">
                <div class="msg_history" id="message-history" active-channel="">

                </div>
                <?=$this->Form->create($messages,['id' => 'form'])?>
                <div class="type_msg d-none" id="type-message" >
                    <div class="input_msg_write">
                        <?=$this->Form->control('message',[
                            'class' => 'write_msg form-control rounded-0',
                            'placeholder' => ucwords('Type a message'),
                            'label' => false,
                            'type' => 'text',
                            'disabled'
                        ])?>
                        <?=$this->Form->control('channel_id',['type' => 'hidden', 'required', 'disabled'])?>
                        <?=$this->Form->control('receiver_id',['type' => 'hidden', 'required', 'disabled'])?>
                        <?=$this->Form->control('sender_id',['type' => 'hidden', 'required', 'value' => ($this->request->getAttribute('identity')->id), 'disabled'])?>
                        <?=$this->Form->control('user_id',['type' => 'hidden', 'required', 'value' => ($this->request->getAttribute('identity')->id), 'disabled'])?>
                        <?=$this->Form->button('<i class="fa fa-paper-plane-o"></i>',[
                            'type' => 'submit',
                            'class' => 'msg_send_btn',
                            'escapeTitle' => false
                        ])?>
                    </div>
                </div>
                <?=$this->Form->end();?>
            </div>
        </div>
        <p class="text-center top_spac"> Design by <a target="_blank" href="https://www.linkedin.com/in/sunil-rajput-nattho-singh/">Sunil Rajput</a></p>

    </div></div>



<script>
    'use strict';
    $(document).ready(function () {

        var socket = new WebSocket("ws://localhost:8080");

        socket.onopen = function() {
            channels();
        };

        socket.onmessage = function(e) {
            const data = JSON.parse(e.data);

            if(data.sender.user_id == id || data.receiver.user_id == id){
                $('#channel-text-'+(data.message.channel_id)+'').text(data.message.message);
                if($('#message-history[active-channel="'+(data.message.channel_id)+'"]').length){
                    messages(data.message);
                    scroll();
                }
            }

        };

        socket.onclose = function(e) {

        };

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: 'http://localhost/CakePHP/messages/add',
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {

                },
            }).done(function (data, status, xhr) {
                $('#channel-text-'+(data.message.channel_id)+'').text(data.message.message);
                socket.send(JSON.stringify(data));
                messages(data.message);
                $('#form')[0].reset();
                scroll();
            }).fail(function (data, status, xhr) {
                console.log(data)
            });
        });

        $(document).on('click', 'div.channels', function (e) {
            var dataTarget = $(this).attr('data-target');
            var sender = $(this).attr('sender');
            var receiver = $(this).attr('receiver');

            if($(this).hasClass('active_chat')){
                e.preventDefault();
                return false;
            }

            $(document).find('div.channels').not(this).removeClass('active_chat');
            $('#message-history').attr('active-channel', parseInt(dataTarget));
            $('#receiver-id').val( (id == sender)? receiver: sender );
            $(this).addClass('active_chat');
            chats(dataTarget);
        });

        function chats(channelId) {
            $.ajax({
                url: 'http://localhost/CakePHP/messages/getMessages/'+(channelId),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#message-history').empty();
                    $('#channel-id').val(channelId);
                },
            }).done(function (data, status, xhr) {
                $.map(data, function (data, key) {
                    messages(data);
                });
            }).fail(function (data, status, xhr) {
                console.log(data)
            }).always(function (data, status, xhr) {
                $('input').prop('disabled', false);
                $('button[type="submit"]').prop('disabled', false);
                $('#type-message').removeClass('d-none').fadeIn(500);
                scroll();
            });
        }

        function channels() {
            $.ajax({
                url: 'http://localhost/CakePHP/channels/getChannels',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend: function (e) {
                    $('#inbox').empty();
                },
            }).done(function (data, status, xhr) {
                $.map(data, function (data,key) {
                    $('#inbox').append('<div class="chat_list channels" data-target="'+(data.id)+'" sender="'+(data.sender.user_id)+'" receiver="'+(data.receiver.user_id)+'"> ' +
                        '<div class="chat_people"> ' +
                        '<div class="chat_img"> ' +
                        '<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> ' +
                        '</div> <div class="chat_ib"> ' +
                        '<h5>'+( (data.sender.user_id == id)? data.receiver.user.username: data.sender.user.username)+'<span class="chat_date">'+(moment(data.messages[0].created).format('MMM D'))+'</span></h5> ' +
                        '<p id="channel-text-'+(data.id)+'">'+(data.messages[0].message)+'</p> ' +
                        '</div> ' +
                        '</div> ' +
                        '</div>')
                });
            }).fail(function (data, status, xhr) {
                console.log(data)
            });
        }

        function messages(data) {
            var message = '';
            if(data.user_id == id){
                message = '<div class="outgoing_msg">' +
                    '<div class="sent_msg"> ' +
                    '<p>'+(data.message)+'</p> ' +
                    '<span class="time_date">'+(moment(data.created).format('Y-m-d'))+'</span> ' +
                    '</div>' +
                    '</div>';
            }else{
                message = '<div class="incoming_msg"> ' +
                    '<div class="incoming_msg_img"> ' +
                    '<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> ' +
                    '</div> <div class="received_msg"> ' +
                    '<div class="received_withd_msg"> ' +
                    '<p>'+(data.message)+'</p> ' +
                    '<span class="time_date">'+(moment(data.created).format('Y-m-d'))+'</span>' +
                    '</div> ' +
                    '</div> ' +
                    '</div>';
            }
            $('#message-history').append(message);
        }

        function scroll() {
            var scroll = document.querySelector('#message-history');
            scroll.scrollTop = scroll.scrollHeight;
        }

        setInterval(function () {
            channels();
        },(1000 * 60));

    });
</script>
</body>
</html>