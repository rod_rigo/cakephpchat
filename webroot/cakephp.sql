/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : cakephp

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 28/09/2023 17:22:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for addresses
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of addresses
-- ----------------------------
INSERT INTO `addresses` VALUES (1, 1, '1', '2023-09-28 14:16:20', '2023-09-28 14:16:20', NULL);
INSERT INTO `addresses` VALUES (2, 1, '1', '2023-09-28 14:16:20', '2023-09-28 14:16:20', NULL);
INSERT INTO `addresses` VALUES (3, 1, '1', '2023-09-28 14:16:20', '2023-09-28 14:16:20', NULL);
INSERT INTO `addresses` VALUES (4, 2, '2', '2023-09-28 14:16:30', '2023-09-28 14:16:30', NULL);
INSERT INTO `addresses` VALUES (5, 2, '2', '2023-09-28 14:16:30', '2023-09-28 14:16:30', NULL);
INSERT INTO `addresses` VALUES (6, 2, '2', '2023-09-28 14:16:30', '2023-09-28 14:16:30', NULL);

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `channel_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of channels
-- ----------------------------
INSERT INTO `channels` VALUES (1, '6515452fb06b4', '2023-09-28 17:19:43', '2023-09-28 17:19:43', NULL);

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `channel_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NULL DEFAULT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `type` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES (1, 1, 2, 'asdas', 0, '2023-09-28 17:19:43', '2023-09-28 17:19:43', NULL);
INSERT INTO `messages` VALUES (2, 1, 2, 'sdfsdf', 0, '2023-09-28 17:20:15', '2023-09-28 17:20:15', NULL);
INSERT INTO `messages` VALUES (3, 1, 1, 'sdfds', 0, '2023-09-28 17:20:19', '2023-09-28 17:20:19', NULL);
INSERT INTO `messages` VALUES (4, 1, 1, 'sss', 0, '2023-09-28 17:20:21', '2023-09-28 17:20:21', NULL);
INSERT INTO `messages` VALUES (5, 1, 1, 'zxcxcx', 0, '2023-09-28 17:20:45', '2023-09-28 17:20:45', NULL);
INSERT INTO `messages` VALUES (6, 1, 2, 'xzczx', 0, '2023-09-28 17:20:49', '2023-09-28 17:20:49', NULL);
INSERT INTO `messages` VALUES (7, 1, 1, 'zxczxcz', 0, '2023-09-28 17:20:53', '2023-09-28 17:20:53', NULL);
INSERT INTO `messages` VALUES (8, 1, 1, 'zxcz', 0, '2023-09-28 17:20:54', '2023-09-28 17:20:54', NULL);
INSERT INTO `messages` VALUES (9, 1, 1, 'zxczxc', 0, '2023-09-28 17:20:56', '2023-09-28 17:20:56', NULL);

-- ----------------------------
-- Table structure for receivers
-- ----------------------------
DROP TABLE IF EXISTS `receivers`;
CREATE TABLE `receivers`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `channel_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of receivers
-- ----------------------------
INSERT INTO `receivers` VALUES (1, 1, 1, '2023-09-28 17:19:43', '2023-09-28 17:19:43', NULL);

-- ----------------------------
-- Table structure for senders
-- ----------------------------
DROP TABLE IF EXISTS `senders`;
CREATE TABLE `senders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `channel_id` bigint NOT NULL,
  `user_id` bigint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of senders
-- ----------------------------
INSERT INTO `senders` VALUES (1, 1, 2, '2023-09-28 17:19:43', '2023-09-28 17:19:43', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'rods', '$2y$10$FUN9c2V98alJCqjscnSd3efO7bKXvAwBYLe23ffN3tcyu2ubUa7Nu', '1111', '2023-09-28 14:16:20', '2023-09-28 15:14:13', NULL);
INSERT INTO `users` VALUES (2, 'admin', '$2y$10$qftjdD0tCF6lggaVZqIQ3O0bdKmhCuCESmxtuQD.527g8EN1t3zuu', '2', '2023-09-28 14:16:30', '2023-09-28 15:13:56', NULL);

SET FOREIGN_KEY_CHECKS = 1;
