<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Collection\Collection;
use Cake\ORM\Query;

/**
 * Channels Controller
 *
 * @property \App\Model\Table\ChannelsTable $Channels
 * @method \App\Model\Entity\Channel[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChannelsController extends AppController
{


    public function getChannels(){
       try{
           $data = $this->Channels->find()
               ->contain([
                   'Senders',
                   'Senders.Users',
                   'Receivers',
                   'Receivers.Users',
                   'Messages' => function(Query $query){
                       return $query->find('all')->where([
                           'messages.is_read =' => intval(0),
                           'messages.user_id !=' => intval($this->request->getAttribute('identity')->id)
                       ]);
                   },
                   'Messages.Users',
               ])
               ->where([
                   'OR' => [
                       'senders.user_id =' => intval($this->request->getAttribute('identity')->id),
                       'receivers.user_id =' => intval($this->request->getAttribute('identity')->id)
                   ]
               ])
                ->join([
                    'alias' => 'message',
                    'table' => 'Messages',
                    'type' => 'LEFT',
                    'conditions' => [
                        'message.channel_id = channels.id',
                    ]
                ])
               ->select(['message.user_id', 'message.message', 'message.type', 'message.is_read', 'message.created'])
               ->group(['channels.id', 'message.channel_id'])
           ->enableAutoFields(true);
           return $this->response->withType('application/json')
               ->withStringBody(json_encode($data));
       }catch (\Exception $exception){
           dd($exception->getMessage());
       }
    }


}
