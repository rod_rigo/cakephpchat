<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Collection\Collection;
use Cake\Datasource\ConnectionManager;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 * @method \App\Model\Entity\Message[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MessagesController extends AppController
{

    public function getMessages($channelId = null){
        $data = $this->Messages->find('all')
            ->where([
                'messages.channel_id =' => $channelId
            ])
            ->order(['messages.id' => 'ASC'])
            ->limit(1000);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function add()
    {
        $message = $this->Messages->newEmptyEntity();
        if ($this->request->is('post')) {
            $connection = ConnectionManager::get('default');
            $connection->begin();
             try{
                 $message = $this->Messages->patchEntity($message, $this->request->getData());

                 $channel = $this->getTableLocator()->get('Channels')->findOrCreate(
                     ['channels.id =' => $this->request->getData('channel_id')],
                     function ($entity){
                         $entity->channel_token = uniqid();
                     }
                 );

                 $sender = $this->getTableLocator()->get('Senders')->findOrCreate(
                     ['senders.channel_id =' => $channel->id],
                     function ($entity) use($channel){
                         $entity->channel_id = $channel->id;
                         $entity->user_id = $this->request->getAttribute('identity')->id;
                     }
                 );

                 $receiver = $this->getTableLocator()->get('Receivers')->findOrCreate(
                     ['receivers.channel_id =' => $channel->id],
                     function ($entity) use($channel){
                         $entity->channel_id = $channel->id;
                         $entity->user_id = $this->request->getData('receiver_id');
                     }
                 );

                 $message->channel_id = $channel->id;

                 if ($this->Messages->save($message)) {
                     $result = (new Collection(['message' => $message->getOriginalValues(), 'sender' => $sender->getOriginalValues(), 'receiver' => $receiver->getOriginalValues()]))->toArray();
                     return $this->response->withStatus(200)->withType('application/json')
                         ->withStringBody(json_encode($result));
                 }else{
                     $result = ['message' => ucwords('The Message Has Not Been Sent'), 'result' => ucwords('error')];
                     return $this->response->withStatus(200)->withType('application/json')
                         ->withStringBody(json_encode($result));
                 }
             }catch (\Exception $exception){
                 dd($exception->getMessage());
                 $connection->rollback();
             }finally{
                 $connection->commit();
             }

        }
        $channels = $this->Messages->Channels->find('list', ['limit' => 200]);
        $users = $this->Messages->Users->find('list', ['limit' => 200]);
        $this->set(compact('message', 'channels', 'users'));
    }

    public function read($channelId = null){
        $connection = ConnectionManager::get('default');
        $this->request->allowMethod(['post']);
        $channel = $this->getTableLocator()->get('Channels')->get($channelId);
        if($this->request->is('post')){
            $connection->begin();
            try{
                if($this->Messages->updateAll(['is_read' => 1],['messages.is_read =' => 0, 'messages.channel_id =' => intval($channel->id), 'messages.user_id !=' => intval($this->request->getAttribute('identity')->id),])){
                    $result = ['message' => ucwords('messages successfully read'), 'result' => ucwords('success')];
                    return $this->response->withStatus(200)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }else{
                    $result = ['message' => ucwords('messages not read'), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }catch (\Exception $exception){
                $connection->rollback();
                dd($exception->getMessage());
            }finally{
                $connection->commit();
            }
        }
    }

    public function unread($channelId = null){
        $this->request->allowMethod(['post']);
        $channel = $this->getTableLocator()->get('Channels')->get($channelId);
        $data = $this->Messages->find()
            ->where([
                'messages.channel_id =' => $channel->id,
                'messages.is_read =' => 0,
                'messages.user_id !=' => intval($this->request->getAttribute('identity')->id),
            ])->count();
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['count' => $data]));
    }

}
