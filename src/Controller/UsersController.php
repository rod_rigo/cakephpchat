<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Validation\Validator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    // in src/Controller/AppController.php
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // for all controllers in our application, make index and view
        // actions public, skipping the authentication check
        $this->Authentication->addUnauthenticatedActions(['login','edit']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function chat()
    {
        $messages = $this->getTableLocator()->get('Messages')->newEmptyEntity();
        $this->viewBuilder()->setLayout('ajax');
        $this->set(compact('messages'));
    }

    public function users(){
        $data = $this->Users->find()
            ->contain([
                'Messages' => function($query){
                    return $query->find('all')->order(['messages.created' => 'DESC'])->limit(1);
                },
            ])
            ->where([
                'users.id !=' => intval($this->request->getAttribute('identity')->id)
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    public function deleted()
    {
        $users = $this->paginate($this->Users->find('all',['withDeleted'])->whereNotNull('users.deleted'));

        $this->set(compact('users'));
    }

    public function messages()
    {
        $this->viewBuilder()->setLayout('ajax');
    }

    // in src/Controller/UsersController.php
    public function logout()
    {
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result && $result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }

    public function login()
    {
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result && $result->isValid()) {
            // redirect to /articles after login success
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'Users',
                'action' => 'chat',
            ]);

            return $this->redirect($redirect);
        }
        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Addresses'],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user,['associated' =>['Addresses']])) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [
                'Addresses'
            ],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

//            $validator = new Validator();
//
//            $addresses = new Validator();
//            $addresses
//                ->requirePresence('address', true)
//                ->notEmptyString('address','Please enter address',false)
//                ->add('address','address',[
//                    'rule' => function($value){
//                        if(strlen($value) == 0){
//                            return ucwords('not have a value!');
//                        }
//                        return true;
//                    }
//                ]);
//
//            $validator->addNestedMany('addresses',$addresses,'Not Have A Address',true);
//
//            $errors = $validator->validate($this->request->getData());
//
//            dd($errors);

            if ($this->Users->save($user,['associated' => ['Addresses']])) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id,[
            'withDeleted',
            'contain' => [
                'Addresses'
            ]
        ]);
        if ($this->Users->restore($user) && $this->Users->Addresses->updateAll(['addresses.deleted' => NULL],['addresses.deleted IS NOT' => NULL, 'addresses.user_id =' => $user->id])) {
            $this->Flash->success(__('The user has been restored.'));
        } else {
            $this->Flash->error(__('The user could not be restored. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
